set :repo_url, 'git@bitbucket.org:Mirkom63/aflex-video.git'
set :application, 'aflex-video'
application = 'aflex-video'
set :rvm_type, :user
set :rvm_ruby_version, '2.6.3'
set :deploy_to, "/home/app/project/aflex-video/"
set :keep_releases, 1
set :default_shell, '/bin/bash -l'


set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/assets public/system }
set :linked_files, %w{config/secrets.yml config/database.yml config/boot.rb}

set :unicorn_config, "#{shared_path}/config/unicorn.rb"
set :unicorn_pid, "#{shared_path}/run/unicorn.pid"



namespace :setup do
  desc 'Загрузка конфигурационных файлов на удаленный сервер'
  task :upload_config do
    on roles :all do
      execute :mkdir, "-p #{shared_path}"
      ['shared/config'].each do |f|
        upload!(f, shared_path, recursive: true)
      end
    end
  end
end



namespace :application do
  desc 'Запуск Unicorn'
  task :start do
    on roles(:app) do
      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec unicorn_rails -c #{fetch(:unicorn_config)} -E #{fetch(:rails_env)} -D"
    end
  end
  desc 'Завершение Unicorn'
  task :stop do
    on roles(:app) do
      execute "if [ -f #{fetch(:unicorn_pid)} ] && [ -e /proc/$(cat #{fetch(:unicorn_pid)}) ]; then kill -9 `cat #{fetch(:unicorn_pid)}`; fi"

      execute "cd #{release_path} && ~/.rvm/bin/rvm default do bundle exec rake db:migrate RAILS_ENV=production"

    end
  end
end


namespace :deploy do

  ask(:message, "Commit message?")


  after :finishing, 'application:stop'
  after :finishing, 'application:start'
  after :finishing, :cleanup


end
