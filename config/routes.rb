Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  #devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/send_form' => 'index#send_form'
  get '/check_devise' => 'index#check_devise'
  root "index#index"
end
