class AddYoutubeId < ActiveRecord::Migration[5.2]
  def change
    add_column :portfolios, :youtube_id, :string
  end
end
