class AddPosition < ActiveRecord::Migration[5.2]
  def change
    add_column :contacts, :position, :integer
    add_column :services, :position, :integer

    add_column :contacts, :ancestry, :string
    add_column :services, :ancestry, :string
    add_column :portfolios, :ancestry, :string
  end
end
