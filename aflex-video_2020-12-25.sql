# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 89.108.65.8 (MySQL 5.7.29-0ubuntu0.18.04.1)
# Схема: aflex-video
# Время создания: 2020-12-25 19:26:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы ar_internal_metadata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ar_internal_metadata`;

CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`)
VALUES
	('environment','production','2020-02-11 18:16:31','2020-02-29 16:57:08');

/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `post` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image_file_name` varchar(255) DEFAULT NULL,
  `image_content_type` varchar(255) DEFAULT NULL,
  `image_file_size` bigint(20) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `ancestry` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;

INSERT INTO `contacts` (`id`, `name`, `post`, `email`, `phone`, `created_at`, `updated_at`, `image_file_name`, `image_content_type`, `image_file_size`, `image_updated_at`, `position`, `ancestry`)
VALUES
	(1,'Алексей Афанасьев','CEO','info@aflex-video.ru','+7-925-866-60-02','2020-02-19 05:14:32','2020-03-01 19:42:31','_MG_9591.jpg','image/jpeg',3355725,'2020-03-01 19:42:29',1,NULL);

/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы mains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mains`;

CREATE TABLE `mains` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `form_email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `mains` WRITE;
/*!40000 ALTER TABLE `mains` DISABLE KEYS */;

INSERT INTO `mains` (`id`, `form_email`, `created_at`, `updated_at`)
VALUES
	(1,'info@aflex-video.ru','2020-02-18 19:18:20','2020-02-29 17:13:01');

/*!40000 ALTER TABLE `mains` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы portfolios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolios`;

CREATE TABLE `portfolios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `vimeo_id` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ancestry` varchar(255) DEFAULT NULL,
  `youtube_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;

INSERT INTO `portfolios` (`id`, `name`, `position`, `vimeo_id`, `created_at`, `updated_at`, `ancestry`, `youtube_id`)
VALUES
	(1,'Видео 1','4',NULL,'2020-02-19 05:27:00','2020-10-22 19:21:35',NULL,'HJ5OBM4UmZw'),
	(2,'Видео 2','3','','2020-02-19 05:27:06','2020-03-02 10:07:57',NULL,'dudg_Qy-BUE'),
	(3,'Видео 3','5','','2020-02-19 05:27:12','2020-03-02 10:07:57',NULL,'WMXOTWwdXMA'),
	(4,'Видео 4','6','','2020-02-19 05:27:17','2020-03-02 10:07:57',NULL,'R7hBx7Iu57I'),
	(5,'Видео 5','1',NULL,'2020-03-02 06:32:33','2020-03-02 10:07:57',NULL,'NFa1ZUwUa8s'),
	(6,'Видео 6','2',NULL,'2020-03-02 10:06:00','2020-03-02 10:12:11',NULL,'-qpAD6wTOU0'),
	(7,'Видео 7',NULL,NULL,'2020-03-02 10:13:04','2020-03-02 10:13:04',NULL,'HX83Dn-OIv0'),
	(8,'Видео 8',NULL,NULL,'2020-03-02 10:14:22','2020-03-02 10:14:22',NULL,'31sGqiPBmxY'),
	(10,'9',NULL,NULL,'2020-11-10 16:54:38','2020-11-10 16:55:00',NULL,'nY1qtbg5iJc'),
	(11,'11',NULL,NULL,'2020-12-02 17:34:50','2020-12-02 17:34:50',NULL,'V_eS657nago');

/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы prices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `prices`;

CREATE TABLE `prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;

INSERT INTO `prices` (`id`, `name`, `price`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'Репортаж','<span>От</span> 130 000 ₽','-Написание сценария<br>\n-Препродакшн<br>\n-Съемка на&nbsp;локации<br>\n-Постпродакшн: монтаж, графическое оформление, цветокоррекция, подбор музыки, озвучивание диктором<br>','2020-02-19 05:09:36','2020-02-19 05:10:32'),
	(2,'Фильм о продукте<br> или компании','<span>От</span> 300 000 ₽','-Маркетинговый анализ продукта или компании<br>\n-Подбор референсов<br>\n-Написание режиссерского сценария и&nbsp;текста для закадрового голоса<br>\n-Осмотр локации режиссером и&nbsp;оператором-постановщиком<br>\n-Создание технического задания на&nbsp;графическое оформление<br>\n-Препродакшн<br>\n-Видеосъемка с&nbsp;применением необходимых технических средств<br>\n-Постпродакшн: монтаж, адаптация графических элементов в&nbsp;2D&nbsp;и&nbsp;3D, саунд-дизайн, цветокоррекция, озвучивание диктором, подбор или создание музыки','2020-02-19 05:10:50','2020-02-19 05:10:50'),
	(3,'Постановочный<br> ролик с участием<br> актерского состава','По договоренности','Объем работ в&nbsp;рамках данной услуги напрямую зависит от&nbsp;нюансов: количества и&nbsp;степени популярности актеров, сложности сценария, количества и&nbsp;расположения локаций для съемки и&nbsp;др.','2020-02-19 05:11:13','2020-02-19 05:11:13');

/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы schema_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;

INSERT INTO `schema_migrations` (`version`)
VALUES
	('20200218183824'),
	('20200218183934'),
	('20200218184055'),
	('20200218184259'),
	('20200218184445'),
	('20200218184646'),
	('20200218184803'),
	('20200218185454'),
	('20200225174708'),
	('20200225174958'),
	('20200229164146');

/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы services
# ------------------------------------------------------------

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image_file_name` varchar(255) DEFAULT NULL,
  `image_content_type` varchar(255) DEFAULT NULL,
  `image_file_size` bigint(20) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `ancestry` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;

INSERT INTO `services` (`id`, `title`, `description`, `created_at`, `updated_at`, `image_file_name`, `image_content_type`, `image_file_size`, `image_updated_at`, `position`, `ancestry`)
VALUES
	(1,'Создание видеокейсов','Отчетные фильмы о&nbsp;проектах, отражающие этапы их&nbsp;реализации и&nbsp;результаты','2020-02-18 18:57:34','2020-02-19 05:03:00','image_34.png','image/png',244662,'2020-02-19 05:01:19',1,NULL),
	(2,'Съемка видео для внутренних коммуникаций','Обучающие, мотивирующие и&nbsp;информирующие ролики, предназначенные для внутреннего использования','2020-02-19 05:01:35','2020-02-19 05:03:00','image_35.png','image/png',191497,'2020-02-19 05:01:35',2,NULL),
	(3,'Освещение мероприятий','Онлайн-трансляции или репортажи, отражающие деятельность компании в&nbsp;рамках крупных или локальных событий','2020-02-19 05:02:02','2020-02-19 05:03:00','image_47.png','image/png',151600,'2020-02-19 05:02:02',3,NULL),
	(4,'Разработка объясняющих видео','Подробное и&nbsp;наглядное описание продукта или услуги с&nbsp;помощью интерактивных инструментов (инфографика, видео в&nbsp;формате 360&deg;, 3D-ролик)','2020-02-19 05:02:22','2020-02-19 05:03:00','image_45.png','image/png',272645,'2020-02-19 05:02:22',4,NULL),
	(5,'Изготовление промороликов','Видеовизитка компании, которая емко и&nbsp;ярко демонстрирует ее&nbsp;ценности и&nbsp;миссию','2020-02-19 05:02:37','2020-02-19 05:03:00','image_44.png','image/png',247991,'2020-02-19 05:02:37',5,NULL),
	(6,'Подготовка коммерческих предложений в видеоформате','Нестандартный формат предложения сотрудничества для предоставления заказчику','2020-02-19 05:02:52','2020-02-19 05:03:00','image_46.png','image/png',385936,'2020-02-19 05:02:51',6,NULL);

/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `encrypted_password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `created_at`, `updated_at`, `admin`)
VALUES
	(1,'info@aflex-video.ru','$2a$11$jqttMd1b3lbASlvDTRXk6eAkULzZ8TMgQov3RBf5R8wDVLMOedCou',NULL,NULL,'2020-02-29 16:41:25','2020-02-25 17:48:43','2020-02-29 16:41:25',1);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
