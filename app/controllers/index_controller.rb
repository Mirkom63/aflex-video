class IndexController < ApplicationController

  def check_devise
    render plain: DeviceDetector.new(request.user_agent).device_type
  end
  def send_form


    html_message='<table cellpadding="10">'
    html_message=html_message+'<tr><td>Имя:</td><td>'+params[:name]+'</td></tr>'
    html_message=html_message+'<tr><td>Телефон:</td><td>'+params[:phone]+'</td></tr>'
    html_message=html_message+'<tr><td>E-mail:</td><td>'+params[:email]+'</td></tr>'
		html_message=html_message+'<tr><td>Вопрос:</td><td>'+params[:question].gsub("\n","<br>")+'</td></tr>'
		html_message=html_message+'</table>'


    SenderMailer.send_form(params[:form_title],Main.find(1).form_email,html_message).deliver_now


  end

  def index

    browser = Browser.new(request.user_agent, accept_language: "en-us")
    @mobile=browser.device.mobile?


  end
end
