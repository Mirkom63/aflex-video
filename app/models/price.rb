class Price < ApplicationRecord


  rails_admin do

    edit do
      field :name do
        label 'Название'
      end
      field :price do
        label 'Цена'
      end
      field :description do
        label 'Описание'
      end


    end
  end


end
