class Contact < ApplicationRecord

  has_attached_file :image, styles: { image: '70x70#'}
  validates_attachment_content_type :image, content_type: %r{\Aimage/.*\Z}


  has_ancestry


  rails_admin do
    nestable_list true

    nestable_tree(
      position_field: :position,
      live_update: true
    )

    edit do
      field :name do
        label 'Имя'
      end
      field :post do
        label 'Должность'
      end

      field :image do
        label 'Фотография'
      end

      field :email do
        label 'E-mail'
      end
      field :phone do
        label 'Телефон'
      end

    end
  end


end
