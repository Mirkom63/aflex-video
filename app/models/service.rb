class Service < ApplicationRecord

  has_ancestry


  has_attached_file :image, styles: { image: '530x350#'}
  validates_attachment_content_type :image, content_type: %r{\Aimage/.*\Z}


  rails_admin do
    nestable_list true

    nestable_tree(
      position_field: :position,
      live_update: true
    )

    edit do
      field :title do
        label 'Заголовок'
      end
      field :description do
        label 'Описание'
      end

      field :image do
        label 'Картинка'
      end

    end
  end

end
