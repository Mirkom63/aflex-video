class Portfolio < ApplicationRecord


  has_ancestry

  rails_admin do
    nestable_list true

    nestable_tree(
      position_field: :position,
      live_update: true
    )

    edit do
      field :name do
        label 'Название'
      end
      field :vimeo_id do
        label 'Youtube ID'
      end
      field :vimeo_id do
        label 'Vimeo ID'
      end

      field :youtube_id do
        label 'YouTube ID'
      end

      field :rutube_id do
        label 'RuTube ID'
      end


    end
  end


end
