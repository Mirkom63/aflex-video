class SenderMailer < ApplicationMailer

  default from: 'sender@aflex-video.ru'

	def send_form(title,email,message)

		@message=message

		mail(to: email, subject: title)

	end


end
