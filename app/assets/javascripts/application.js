// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs

//= require plugin/jquery.scrollTo.min.js
//= require plugin/jquery.mask
//= require plugin/owl.carousel
//= require plugin/lazysizes


$(document).ready(function(){


  var ar_check_iframe=[];

  $(document).scroll(function(e){
    check_look_iframe();
  });
  check_look_iframe();

  function check_look_iframe(){
    var scroll=$(window).scrollTop();
    var height=$(window).height();
    var max_position=scroll+height;

    $('.js-iframe-portfolio').each(function(){
      var src=$(this).data('src');
      var iframe_position=$(this).offset().top;
      if(iframe_position>scroll && iframe_position<max_position){

        if(ar_check_iframe.indexOf( src+'_'+$(this).index() ) == -1){
          ar_check_iframe.push(src+'_'+$(this).index());

          $(this).attr('src',src);


        }

      }

    });
  }




  $('.head_menu_mobile').click(function(){
    $('.block_menu_mobile').addClass('block_menu_mobile_show');
  });
  $('.button_close').click(function(){
    $('.block_menu_mobile').removeClass('block_menu_mobile_show');
  });

  $(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $(".block_menu_mobile"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0 // и не по его дочерним элементам
      ) {

      $('.block_menu_mobile').removeClass('block_menu_mobile_show');

    }
  });

  $('.block_menu_mobile a').click(function(){
    $('.block_menu_mobile').removeClass('block_menu_mobile_show');
  });

  $('.block_menu_mobile').css('transition','0.5s');

  if($('input[name="mobile"]').val()=='true'){
    var slider=$('.block_price_slider').owlCarousel({
  		loop:false,
  		nav: false,
  		dots: false,
  		items:1,
  		navSpeed : 800,
  	});
    slider.trigger('to.owl.carousel', [1,0]);

  }

  $(document).on('keyup','.input_error',function(){
		$(this).removeClass('input_error');
	});

	$('.template_form_agree input').change(function(){
		$(this).parent().find('.ui-checkboxradio-icon').removeClass('input_error');
	});

	$('.form').submit(function(e){
		var form=$(this);
		e.preventDefault();
		var send_form=true;

		$(this).find('.js-check-form').each(function(){
			if($(this).val().length==0){
				send_form=false;
				$(this).addClass('input_error');
			}else{
				$(this).removeClass('input_error');
			}
		});

		// var checked=$(this).find('.template_form_agree input').prop('checked');
    //
		// if(checked==false){
		// 	send_form=false;
		// 	$(this).find('.template_form_agree .ui-checkboxradio-icon').addClass('input_error');
		// }

		if(send_form==true){

			form.find('.form_thanks').css('display','flex');

			$.ajax({
				type     :'POST',
				cache    : false,
				data: form.serialize(),
				url  : "/send_form",
				success  : function(response) {



				}
			});
		}

	});



  $('input[type="tel"]').mask('0 (000) 000-00-00');


  $('.pop_up_close').click(function(){
    $('.pop_up_showreel').hide();
    $('.body').css('overflow','auto');


    var video = $("#video_showreel").attr("src");
    $("#video_showreel").attr("src","");
    $("#video_showreel").attr("src",video);


  });
  $('.js-open-showreel').click(function(){
    $('.pop_up_showreel').show();
    $('.body').css('overflow','hidden');
  });


  $('.js-scroll').click(function(){
    var anchor=$(this).attr('anchor');
    $(document).scrollTo($('.'+anchor), 800,{offset:-50});
  });


  //jQuery("#bgndVideo").YTPlayer();


  $(window).scroll(function(){
    var scroll=$(window).scrollTop()*0.5;

    $('.block_price_rectange_first .block_price_rectange_box').css('transform','rotate('+scroll+'deg)');
    $('.block_price_rectange_center .block_price_rectange_box').css('transform','rotate('+scroll*0.7+'deg)');
    $('.block_price_rectange_last .block_price_rectange_box').css('transform','rotate('+scroll*0.3+'deg)');


  });

  $('.logo_showreel_hover').mouseover(function(){
    $('.hover_showreel').show();
  });
  $('.logo_showreel_hover').mouseleave(function(){
    $('.hover_showreel').hide();
  });

  var set_position=true;
  var set_position_hover_showreel;

  if($('input[name="mobile"]').val()=='false'){
    $(document).mousemove(function(e){

      $('.hover_showreel').css('transform','translateX('+(e.clientX+10)+'px) translateY('+(e.clientY+10)+'px)');

      if(set_position==true){

        var posX=(e.clientX-($(window).width()/2))*0.1;
        var posY=(e.clientY-($(window).height()/2))*0.1;

        $('.block_price_rectange_first').css('transform','translateX('+posX*1+'px) translateY('+posY+'px)');
        $('.block_price_rectange_center').css('transform','translateX('+posX*0.7+'px) translateY('+posY*0.7+'px)');
        $('.block_price_rectange_last').css('transform','translateX('+posX*0.3+'px) translateY('+posY*0.3+'px)');

        $('.camera img').css('transform','translateX('+posX*0.5+'px) translateY('+posY*0.5+'px) rotate(45deg)');

        $('.point_services_left img').css('transform','translateX('+posX*0.2*-1+'px) translateY('+posY*0.2*-1+'px)');
        $('.point_services_right img').css('transform','translateX('+posX*0.2+'px) translateY('+posY*0.2+'px)');

        $('.point_services_image').css('transform','rotateY('+posX*0.07+'deg) rotateX('+posY*0.07+'deg)');

        set_position=false;
      }
    });

    function set_time_position(){
      setTimeout(function(){
        set_position=true;
        set_time_position();
      },20);
    }

    set_time_position();
  }

});
